% dropAny(+Elem,+List,-OutList)
dropAny(X,[X|T],T). 
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).

% dropFirst(+Elem,+List,-OutList)
dropFirst(X,[X|T],T):- !.
dropFirst(X,[H|Xs],[H|L]):- dropFirst(X,Xs,L).


% dropLast(+Elem,+List,-OutList)
dropLast(X,[H|Xs],[H|L]):-dropLast(X,Xs,L), !.
dropLast(X,[X|T],T).



% dropAll(+Elem,+List,-OutList)
dropAll(X, [], []).
dropAll(X,[X|T],L) :- dropAll(X, T, L), !.
dropAll(X,[H|T],[H|L]):- dropAll(X, T, L).

% fromList(+List,-Graph)
fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).

% fromCircList(+List,-Graph)
fromCircList([_],[]).
fromCircList([H1,H2|T],[e(H1,H2)|L]):- fromCircList([H2|T],L).
fromCircList([HN|[]], [e(HN, H1)|[]]) :- fromCircList([H1|T], L), !.  

% dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node 
% use dropAll
dropNode(G,N,O):- dropAll(e(N,_),G,G2), dropAll(e(_,N),G2,O).

% reaching(+Graph, +Node, -List)
% all the nodes that can be reached in 1 step from Node 
reaching(G, N, L):- reaching(G, N, L, []).
reaching([e(N, X)|T], N, L, Temp):- append([X], Temp, NewL), reaching(T, N, L, NewL), !.
reaching([e(X1, X)|T], N, L, Temp):- reaching(T, N, L, Temp).
reaching([], N, L, Temp):- L = Temp.

% anypath(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1
anypath([e(N1, N)|T], N1, N2, LP):- anypath(T, N, N2, CurrentLP), append([e(N1, N)], CurrentLP, LP), member(e(_, N2), LP).
anypath([e(N, N2)|T], N1, N2, LP):- anypath(T, N1, N2, CurrentLP), append([e(N, N2)], CurrentLP, LP), member(e(_, N), LP).
anypath([H|T], N1, N2, LP):- anypath(T, N1, N2, LP).
anypath([], N1, N2, LP):- LP = [].


% allreaching(+Graph, +Node, -List)
% all the nodes that can be reached from Node 
% Suppose the graph is NOT circular!
% Use findall and anyPath!
allreaching(Graph, Node, List):- findall(Destination, anypath(Graph, Node, Destination, ListPath), List).