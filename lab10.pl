

% Part 1


% search(+Elem, +List)
search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).

% search2(+Elem, +List)
% looks for two consecutive occurrences of Elem
search2(X,[X,X|_]). 
search2(X,[_|Xs]):-search2(X,Xs).

% search_two(+Elem, +List)
% looks for two occurrences of Elem with an element in between!
search_two(X,[X,Y,X|_]).
search_two(X,[_|Xs]):-search_two(X,Xs).

% search_anytwo(+Elem, +List)
% looks for any Elem that occurs two times
search_anytwo(X,[X|Xs]):- search(X,Xs).
search_anytwo(X,[_|Xs]):-search_anytwo(X,Xs).

% size(+List, -Size)
% size will contain the number of elements in List
size([],0).
size([_|T],M) :- size(T,N), M is N+1.

%sum(+FirstAddend, +SecondAddend, -Result)
sum(X,zero,X).
sum(X,s(Y),s(Z)):-sum(X,Y,Z).

% size2(+List, -Size)
% size2 will contain the number of elements in List
% written using notation zero, s(zero), s(s(zero))..
size2([], zero).
size2([_|T],M) :- size2(T,N), sum(N, s(zero), M).


% Part 2


% sum(+List, -Sum)
% return the sum of the element in the list.
sum([], 0).
sum([H|T],M) :- sum(T,N), M is H + N.

% sumRelational(+List, -Sum)
% return the sum of the element in the list
% written using notation zero, s(zero), s(s(zero))..
sumRelational([], zero).
sumRelational([H|T],M) :- sumRelational(T,N), sum(N, H, M).

% average(+List, -Average)
% it uses average(List,Count,Sum,Average)
average(List,A) :- average(List,0,0,A). average([],C,S,A) :- A is S/C. average([X|Xs],C,S,A) :-
C2 is C+1,
S2 is S+X, average(Xs,C2,S2,A).

% max(+List, -Max)
% Max is the biggest element in List
% suppose the list has at least one element
max([X],X).
max([X|Xs],M) :- max(Xs,M), M >= X.
max([X|Xs],X) :- max(Xs,X), X > M.


% Part 3

% same(+List1, +List2)
% are the two lists the same?
same([],[]).
same([X|Xs],[X|Ys]):- same(Xs,Ys).

% all_bigger(+List1, +List2)
% all elements in List1 are bigger than those in List2, 1 by 1 
% example: all_bigger([10,20,30,40],[9,19,29,39]).
all_bigger([], []).
all_bigger([X|Xs],[Y|Ys]) :- X > Y, all_bigger(Xs, Ys).

% sublist(+List1, +List2)
% List1 should be a subset of List2 
% example: sublist([1,2],[5,3,2,1]).
sublist([], List2).
sublist([X|Xs], List2) :- search(X, List2), sublist(Xs, List2).


% Part 4
 

% seq(+N, +List)
% example: seq(5,[0,0,0,0,0]).
seq(0,[]).
seq(N,[0|T]):- N > 0, N2 is N-1, seq(N2,T).

% dec(+N, -M).
% returns the N decremented 
dec(s(N), M) :- M = N.

% seqRelational(+N, +List)
% example: seqRelational(s(s(zero)),[0, 0]).
seqRelational(zero, []).
seqRelational(N,[0|T]):- dec(N,N2), seqRelational(N2, T).

% seqR(+N, +List)
% example: seqR(4,[4,3,2,1,0]).
seqR(N,[N]):- N == 0.
seqR(N,[N|T]):- N > 0, N2 is N-1, seqR(N2,T).

% seqRRelational(+N, +List)
% example: seqRRelational(s(s(zero)),[s(s(zero)), s(zero), zero]).
seqRRelational(N,[N]):- N == zero.
seqRRelational(N,[N|T]):- dec(N,N2), seqRRelational(N2,T).

% seqR2(+N, +List)
% example: seqR2(4,[0,1,2,3,4]).
seqR2(N,[M|T]):- N > 0, M == 0, N2 is M+1, seqR2(N,T,N2).
seqR2(N,[N],N).
seqR2(N,[M|T],N2):- M == N2, NewN2 is N2+1, seqR2(N,T,NewN2).


% seqR2Relational(+N, +List)
% example: seqR2Relational(s(s(zero)),[zero, s(zero), s(s(zero))]).
seqR2Relational(N,[M|T]):- M == zero, N2 = s(M), seqR2Relational(N,T,N2).
seqR2Relational(N,[N],N).
seqR2Relational(N,[M|T],N2):- M == N2, NewN2 = s(N2), seqR2Relational(N,T,NewN2).


% Other Exercises

% inv(+List, +List)
% example: inv([1,2,3],[3,2,1]).
inv([H|T], List2):- inv(T, [H], List2).
inv([], InvList, List2):- same(InvList, List2).
inv([H|T], InvList, List2):- inv(T, [H|InvList], List2).


% double(+List, -List)
% suggestion: remember predicate append/3 
% example: double([1,2,3],[1,2,3,1,2,3]).
double(List,Result):- append(List, List, Result).

% times(+List, +N, -List)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).
times(List, N, Result):- times(List, N, Result, []).
times(List, 0, Result, Temp):- Result = Temp.
times(List, N, Result, Temp):- N > 0, M is N-1, append(Temp, List, NewResult), times(List, M, Result, NewResult).


% proj(List,List)
% example: proj([[1,2],[3,4],[5,6]],[1,3,5]).
proj(List, Result):- proj(List, Result, []).
proj([], Result, Temp):- Result = Temp.
proj([[H|T]|Others], Result, Temp):- append(Temp, [H], NewResult), proj(Others, Result, NewResult).
